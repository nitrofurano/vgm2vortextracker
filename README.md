# vgm2vortextracker

sdlBasic converting scripts for converting .vgm files to VortexTracker’s .txt format:
- Vgm2VortexTrackerTxtAy.sdlbas	- converts AY-3-8910-based .vgm files to VortexTracker’s .txt format
- Vgm2VortexTrackerTxtSn.sdlbas	- converts SN76489-based .vgm files to VortexTracker’s .txt format
- Vgm2ModTxtAyK.sdlbas - converts AY-3-8910+K051649-based .vgm files to .mod.txt format (.mod.txt isn’t a popular format yet - it’s actually a huge feature lack on most of trackers not supporting yet txt-based formats)

usage:
sdlBrt scriptname.sdlbas filename.vgm [displayfrequency] [debug]
- [displayfrequency]: 0, -ntsc, 1, -pal
- [debug]: 0, 1, -debug

There are still persisting bugs - be welcome fixing, improving, forking, etc.
