# vgm2txttrackers

sdlBasic converting scripts for converting .vgm files to .txt-based trackers:
- VortexTracker:
  - Vgm2VortexTrackerTxtAy.sdlbas	- converts AY38910-based .vgm files to VortexTracker’s .txt format
  - Vgm2VortexTrackerTxtSn.sdlbas	- converts SN76489-based .vgm files to VortexTracker’s .txt format
- FamiTracker:
  - Vgm2FamiTrackerTxtAy38910K051649.sdlbas - converts AY38910+K051649-based .vgm files to FamiTracker’s .txt format 
  - Vgm2FamiTrackerTxtAy38910.sdlbas        - converts AY38910-based .vgm files to FamiTracker’s .txt format
  - Vgm2FamiTrackerTxtC03051.sdlbas         - converts C03051-based .vgm files to FamiTracker’s .txt format 
  - Vgm2FamiTrackerTxtC03051x2.sdlbas       - converts C03051-based .vgm files to FamiTracker’s .txt format (dual chip)
  - Vgm2FamiTrackerTxtSaa1099.sdlbas        - converts SAA1099-based .vgm files to FamiTracker’s .txt format 
  - Vgm2FamiTrackerTxtSn76489.sdlbas        - converts SN76489-based .vgm files to FamiTracker’s .txt format 
- ModPlugTracker (and OpenMPT):
  - Vgm2ModTxtAyK.sdlbas - converts AY38910+K051649-based .vgm files to .mod.txt format (.mod.txt isn’t a popular format yet - it’s actually a huge feature lack on most of trackers not supporting yet txt-based formats)

usage:
sdlBrt scriptname.sdlbas filename.vgm [displayfrequency] [debug]
- [displayfrequency]: 0, -ntsc, 1, -pal
- [debug]: 0, 1, -debug

There are still persisting bugs - be welcome fixing, improving, forking, etc.
